const path = require('path');
const fs = require('fs');
const webpack = require('webpack');


const isDevelopment = !process.env.production;
const assetsPath = path.join(__dirname, '/public');

const config = {
  entry: ['babel-polyfill', './src/js/index.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: [/node_modules/],
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }]
    },]
  },
  plugins: [],
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 5000
  }
};

if (isDevelopment) {
  fs.readdirSync(assetsPath)
    .map((fileName) => {
    if (['.css', '.js'].includes(path.extname(fileName))) {
    return fs.unlinkSync(`${assetsPath}/${fileName}`);
  }

  return '';
});
} else {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  );
}

module.exports = config;