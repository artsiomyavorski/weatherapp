import DOMREFS from './dom'
import { API_KEY, API_URL } from './constants'

export const fetchWeatherByCords = async ({lat, lon}) => {
  try {
    const url = `${API_URL}?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`
    return await (await fetch(url)).json()

  } catch(e) {
    console.log(e)
  }

}

export const renderWeatherProps = (main, name) => {
  DOMREFS.weatherPanel.innerText = name ? name : 'Unknown place'

  Object.keys(main).map( prop => {

    const container = document.createElement('div')
    container.className = 'weather-prop'
    const headline = document.createElement('span')
    headline.innerText = prop
    const value = document.createElement('span')
    value.innerText = main[prop]
    container.appendChild(headline)
    container.appendChild(value)

    return DOMREFS.weatherPanel.appendChild(container)
  })
}

export const isNumber = (n) => !isNaN(parseFloat(n)) && isFinite(n)