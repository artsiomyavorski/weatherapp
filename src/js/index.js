import {API_KEY, API_URL} from './constants'
import {fetchWeatherByCords, renderWeatherProps, isNumber} from './helpers'
import DOMREFS from './dom'


const onButtonClick = () => {
  function findPositionFailed() {
    console.log('position not found')
  }

  function foundPosition(position) {
    fetchWeatherByCords({
      lat: position.coords.latitude,
      lon: position.coords.longitude
    })
      .then(({main, name}) => {
          renderWeatherProps(main, name)
        }
      )
  }

  navigator.geolocation.getCurrentPosition(foundPosition, findPositionFailed)
}

const onCustomButtonClick = () => {
  let customCords = {
    lat: DOMREFS.lat.value,
    lon: DOMREFS.lon.value,
  }


  if (isNumber(customCords.lat) && isNumber(customCords.lon)) {
    fetchWeatherByCords(customCords).then(({main, name}) => {
        renderWeatherProps(main, name)
      }
    )
  } else {
    DOMREFS.weatherPanel.innerText = 'Enter valid cords'
  }


}

DOMREFS.getWeatherBtn.addEventListener("click", onButtonClick);
DOMREFS.getCustomWeatherBtn.addEventListener("click", onCustomButtonClick);










