export default {
  getWeatherBtn: document.querySelector('.get-weather'),
  getCustomWeatherBtn: document.querySelector('.get-custom-weather'),
  weatherPanel: document.querySelector('.weather-panel'),
  lat: document.querySelector(".lat-cords"),
  lon: document.querySelector(".lon-cords"),
}